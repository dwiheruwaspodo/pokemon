import { createSlice } from "@reduxjs/toolkit";
import { TPokemon } from "types/pokemon";

interface InitialState {
  myPokemons: TPokemon[];
  nickname: string;
  pokemonCatch: TPokemon | null;
}

const initialState: InitialState = {
  myPokemons: [],
  nickname: "",
  pokemonCatch: null,
};

const pokemonSlice = createSlice({
  name: "my-pokemon",
  initialState,
  reducers: {
    clear: () => initialState,
    setMyPokemon: (state, action) => {
      state.myPokemons = action.payload;
    },
    setNickName: (state, action) => {
      state.nickname = action.payload;
    },
    setPokemonCatch: (state, action) => {
      state.pokemonCatch = action.payload;
    },
  },
});

export const actions = {
  ...pokemonSlice.actions,
};

export const reducer = pokemonSlice.reducer;
