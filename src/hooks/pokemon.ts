import { gql, useLazyQuery } from "@apollo/client";
import { useErrorMessage } from ".";
import { ResponsePokemonDetail, ResponsePokemonList } from "types/pokemon";

/**
 *
 * POKEMON LIST
 */
const POKEMON_LIST = gql`
  query pokemons($offset: Int, $limit: Int) {
    pokemons(offset: $offset, limit: $limit) {
      count
      next
      previous
      nextOffset
      prevOffset
      params
      status
      message
      results {
        id
        url
        name
        image
        artwork
        dreamworld
      }
    }
  }
`;

interface PokemonListProps {
  offset: number;
}

export const usePokemonList = ({ offset }: PokemonListProps) => {
  const [fetchMore, { loading, error, data: response }] =
    useLazyQuery<ResponsePokemonList>(POKEMON_LIST, {
      notifyOnNetworkStatusChange: true,
      errorPolicy: "all",
      fetchPolicy: "cache-and-network",
      variables: {
        offset: offset,
        limit: 10,
      },
    });

  useErrorMessage(error);

  return {
    fetchMore,
    response,
    loading,
  };
};

/**
 *
 * POKEMON DETAIL
 */

const POKEMON_DETAIL = gql`
  query pokemon($name: String!) {
    pokemon(name: $name) {
      id
      abilities {
        ability {
          url
          name
        }
        is_hidden
        slot
      }
      name
      species {
        url
        name
      }
      message
      weight
      height
      held_items {
        item {
          url
          name
        }
      }
      stats {
        base_stat
        effort
        stat {
          url
          name
        }
      }
      types {
        slot
        type {
          url
          name
        }
      }
    }
  }
`;

interface PokemonDetailProps {
  name: string;
}

export const usePokemonDetail = ({ name }: PokemonDetailProps) => {
  const [fetchDetail, { loading: loadingDetail, error, data: responseDetail }] =
    useLazyQuery<ResponsePokemonDetail>(POKEMON_DETAIL, {
      notifyOnNetworkStatusChange: true,
      errorPolicy: "all",
      fetchPolicy: "cache-and-network",
      variables: {
        name: name,
      },
    });

  useErrorMessage(error);

  return {
    fetchDetail,
    responseDetail,
    loadingDetail,
  };
};
