import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { ApolloError } from "@apollo/client";
import { AppDispatch } from "states/store";
import { toastr } from "react-redux-toastr";

export const useAppDispatch = (): AppDispatch => useDispatch<AppDispatch>();

export const useErrorMessage = (error: ApolloError | Error | undefined) => {
  useEffect(() => {
    if (error) {
      toastr.error("", error.message);
    }
    // eslint-disable-next-line
  }, [error]);
};
