import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import ReduxToastr from "react-redux-toastr";
import { PersistGate } from "redux-persist/integration/react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";

import "react-redux-toastr/lib/css/react-redux-toastr.min.css";
import "styles/globals.scss";
import { store, persistor } from "states/store";
import Router from "components/Pages/Router";
import reportWebVitals from "./reportWebVitals";

const backEndUrl =
  typeof process.env.REACT_APP_BACKEND_URL === "string"
    ? process.env.REACT_APP_BACKEND_URL
    : "";

declare global {
  interface Window {
    ResizeObserver: any;
  }
}

const client = new ApolloClient({
  uri: `${backEndUrl}/api/graphql`,
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ApolloProvider client={client}>
        <React.StrictMode>
          <Router />
          <ReduxToastr />
        </React.StrictMode>
      </ApolloProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
reportWebVitals();
