import React, { FC } from "react";
import { isEmpty } from "lodash";
import { TPokemon } from "types/pokemon";
import s from "shortid";
import { shallowEqual, useSelector } from "react-redux";
import { RootState } from "states/store";
import MyPokemonCard from "components/Molecules/MyPokemonCard";
import { SubTitle } from "components/Pages/Pokemons";

const MyPokemonList: FC = () => {
  const { myPokemons } = useSelector(
    (state: RootState) => ({
      myPokemons: state.pokemons.myPokemons,
    }),
    shallowEqual
  );

  return (
    <>
      {!isEmpty(myPokemons) ? (
        <>
          <div className="columns is-multiline mt-6">
            {myPokemons.map((pokemon: TPokemon) => (
              <MyPokemonCard pokemon={pokemon} key={s.generate()} />
            ))}
          </div>
        </>
      ) : (
        <div className="has-text-centered mt-6 pt-6">
          <SubTitle>Your bucket is empty :( </SubTitle>
        </div>
      )}
    </>
  );
};

export default MyPokemonList;
