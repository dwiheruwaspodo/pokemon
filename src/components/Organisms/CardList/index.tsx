import React, { FC, useEffect, useState } from "react";
import { slice, concat, isEmpty } from "lodash";
import { usePokemonList } from "hooks/pokemon";
import Spinner from "components/Atoms/Spinner";
import { TPokemonItem } from "types/pokemon";
import s from "shortid";
import CardItem from "components/Molecules/CardItem";

const CardList: FC = () => {
  const [offset, setOffset] = useState<number>(1);
  const [loadMore, setLoadMore] = useState<boolean>(false);
  const [pokemonList, setPokemonList] = useState<TPokemonItem[]>([]);
  const { fetchMore, response, loading } = usePokemonList({ offset: offset });

  // fetch pokemon detail
  useEffect(() => {
    fetchMore();
  }, [fetchMore]);

  // slice response
  useEffect(() => {
    if ((response && isEmpty(pokemonList)) || (response && loadMore)) {
      const newList = concat(pokemonList, slice(response.pokemons.results));
      setPokemonList(newList);
      if (loadMore) setLoadMore(false);
    }
  }, [response, pokemonList, setPokemonList, loadMore]);

  const clickLoadMore = () => {
    if (response) setOffset(response.pokemons.nextOffset);
    setLoadMore(true);
  };

  return (
    <>
      {loading && (
        <div className="has-text-centered">
          <Spinner />
        </div>
      )}

      {!isEmpty(pokemonList) && (
        <>
          <div className="columns is-multiline">
            {pokemonList.map((pokemon: TPokemonItem) => (
              <CardItem pokemon={pokemon} key={s.generate()} />
            ))}
          </div>
          <div className="column">
            <button
              className="button is-primary is-bold is-fullwidth"
              style={{
                borderRadius: "1rem",
                marginTop: "1rem",
                marginBottom: "1rem",
              }}
              onClick={() => clickLoadMore()}
            >
              Fetch More
            </button>
          </div>
        </>
      )}
    </>
  );
};

export default CardList;
