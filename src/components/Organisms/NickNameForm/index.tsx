import React, { FC, useState } from "react";
// import { TPokemon } from "types/pokemon";
// import s from "shortid";
import Modal from "components/Atoms/Modal";
import { shallowEqual, useSelector } from "react-redux";
import { RootState } from "states/store";
import { TPokemon } from "types/pokemon";
import { toastr } from "react-redux-toastr";
import { actions } from "states/slice";
import { useAppDispatch } from "hooks";

interface NickNameFormProps {
  isOpen: boolean;
  onClose: () => void;
}
const NickNameForm: FC<NickNameFormProps> = ({ isOpen, onClose }) => {
  const dispatch = useAppDispatch();
  const [nickName, setNickName] = useState<string>("");
  // check redux
  const { myPokemons, pokemonCatch } = useSelector(
    (state: RootState) => ({
      myPokemons: state.pokemons.myPokemons,
      pokemonCatch: state.pokemons.pokemonCatch,
    }),
    shallowEqual
  );

  // save pokemon
  const putToBucket = () => {
    let tempPokemon: TPokemon = JSON.parse(JSON.stringify(pokemonCatch));
    tempPokemon.nickname = nickName;

    let tempMyPokemon = Object.assign([], myPokemons);
    tempMyPokemon.push(tempPokemon);

    dispatch(actions.setMyPokemon(tempMyPokemon));
    toastr.success("", "Check your bucket ~");
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <div className="field has-addons ml-3 mr-3">
        <div className="control is-expanded">
          <input
            className="input"
            type="text"
            placeholder="Nickname your pokemon!"
            onChange={(e) => setNickName(e.target.value)}
          />
        </div>
        <div className="control">
          <button
            className="button is-success"
            onClick={() => {
              putToBucket();
              dispatch(actions.setPokemonCatch(null));
              onClose();
            }}
          >
            Add
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default NickNameForm;
