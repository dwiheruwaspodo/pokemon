import React, { FC } from "react";
import { TPokemon } from "types/pokemon";
import s from "shortid";

interface BasicInfoProps {
  pokemon: TPokemon;
}
const BasicInfo: FC<BasicInfoProps> = ({ pokemon }) => {
  return (
    <div className="column mt-0 ml-0 pr-3">
      <h1 className="title is-4 mb-1">Basic Info</h1>
      <table
        className="table is-fullwidth is-striped"
        style={{ borderRadius: "1rem" }}
      >
        <tbody>
          <tr>
            <td>Height</td>
            <td>{pokemon.weight}</td>
          </tr>
          <tr>
            <td>Weight</td>
            <td>{pokemon.weight}</td>
          </tr>
          <tr>
            <td>Ability</td>
            <td className="pl-1 pt-0">
              <div className="content is-small mt-0">
                <ul>
                  {pokemon.abilities.map((value) => {
                    return <li key={s.generate()}>{value.ability.name}</li>;
                  })}
                </ul>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default BasicInfo;
