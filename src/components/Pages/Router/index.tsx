import { FC, Suspense, lazy } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";

import LoadingPage from "components/Pages/LoadingPage";
const Pokemons = lazy(async () => await import("components/Pages/Pokemons"));
const Pokemon = lazy(async () => await import("components/Pages/Pokemon"));
const MyPokemons = lazy(
  async () => await import("components/Pages/MyPokemons")
);

const Router: FC = () => (
  <BrowserRouter>
    <Suspense fallback={<LoadingPage />}>
      <Switch>
        <Route exact path="/" component={Pokemons} />
        <Route exact path="/pokemon/:name" component={Pokemon} />
        <Route exact path="/my-pokemon" component={MyPokemons} />
      </Switch>
    </Suspense>
  </BrowserRouter>
);

export default Router;
