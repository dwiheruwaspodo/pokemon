import React from "react";
import { render, screen } from "@testing-library/react";
import Pokemons from "./index";

test("View Title Pokemon", () => {
  render(<Pokemons />);
  const linkElement = screen.getByText(/Pokemon/i);
  expect(linkElement).toBeInTheDocument();
});
