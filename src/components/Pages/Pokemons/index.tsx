import React, { FC, ReactNode } from "react";
import Layout from "components/Templates/Layout";
import CardList from "components/Organisms/CardList";
import styled from "@emotion/styled";
import NavPokemons from "components/Atoms/NavPokemons";

interface ComponentProps {
  className?: string;
  children: ReactNode;
}

const SubTitleComponent: FC<ComponentProps> = ({ className, children }) => (
  <p className={className}>{children}</p>
);

const TitleComponent: FC<ComponentProps> = ({ className, children }) => (
  <h1 className={className}>{children}</h1>
);

export const DivComponent: FC<ComponentProps> = ({ className, children }) => (
  <div className={className}>{children}</div>
);

export const SubTitle = styled(SubTitleComponent)`
  color: #4a4a4a;
`;

export const Title = styled(TitleComponent)`
  color: #363636;
`;

export const Content = styled(DivComponent)`
  position: relative;

  .image-ava {
    cursor: pointer;
    max-height: 350px;
    border: 0.5rem solid #00d1b2;
    border-radius: 50%;

    @media screen and (max-width: 480px) {
      height: 250px;
    }
  }
`;

const Pokemons: FC = () => {
  return (
    <Layout>
      <Content>
        <NavPokemons />
        <div className="column has-text-centered mb-6">
          <Title className="title">Pokemon</Title>
          <SubTitle className="is-success has-text-centered">
            Select monster bellow, <br /> then catch she if you can!
          </SubTitle>
        </div>
        <CardList />
      </Content>
    </Layout>
  );
};

export default Pokemons;
