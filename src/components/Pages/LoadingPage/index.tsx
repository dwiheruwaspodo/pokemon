import { FC } from "react";
import ballImg from "assets/images/pokeball.png";

const LoadingPage: FC = () => {
  return (
    <main
      style={{
        color: "hotpink",
        width: "100vw",
        height: "100vh",
        margin: "auto",
        position: "relative",
      }}
    >
      <img
        src={ballImg}
        alt="pokeBall"
        style={{
          width: "10rem",
          height: "auto",
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      />
    </main>
  );
};

export default LoadingPage;
