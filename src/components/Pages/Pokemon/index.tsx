import React, { FC, useEffect, useState } from "react";
import { usePokemonDetail } from "hooks/pokemon";
import Layout from "components/Templates/Layout";
import Spinner from "components/Atoms/Spinner";
import Nav from "components/Atoms/Nav";
import Type from "components/Molecules/Type";
import Stats from "components/Molecules/Stats";
import BasicInfo from "components/Organisms/BasicInfo";
import NickNameForm from "components/Organisms/NickNameForm";
import { Content, DivComponent } from "components/Pages/Pokemons";
import { useParams } from "react-router-dom";
import { isNil } from "lodash";
import { useAppDispatch } from "hooks";
import { TPokemon } from "types/pokemon";
import { actions } from "states/slice";
import { toastr } from "react-redux-toastr";
import styled from "@emotion/styled";

interface TParams {
  name: string;
}

export const InfoSection = styled(DivComponent)`
  background-color: #f5f5f5;
  border-radius: 1rem;
  margin: 0 1rem;
`;

const Pokemon: FC = () => {
  const dispatch = useAppDispatch();
  const { name } = useParams<TParams>();
  const [isOpenNickName, setOpenNickName] = useState<boolean>(false);
  const { fetchDetail, responseDetail, loadingDetail } = usePokemonDetail({
    name: name,
  });

  // get pokemon detail
  useEffect(() => {
    fetchDetail();
  }, [fetchDetail]);

  // catch pokemon
  const catchPokemon = (pokemon: TPokemon) => {
    let pokemonCatch = JSON.parse(JSON.stringify(pokemon));

    let rand = Math.random();
    if (rand < 0.5) {
      toastr.error("", "Try Again, dude :(");
    } else {
      setOpenNickName(true);
      dispatch(actions.setPokemonCatch(pokemonCatch));
    }
  };

  return (
    <Layout>
      <Content>
        <Nav />

        {loadingDetail && (
          <div className="has-text-centered">
            <Spinner />
          </div>
        )}

        {!isNil(responseDetail) && (
          <>
            <div className="column info-ava has-text-centered">
              <h1 className="title title-header mb-1">
                {responseDetail.pokemon.name}
              </h1>
              <p className="mb-4">
                #{("000" + responseDetail.pokemon.id).slice(-4)}
              </p>
              <img
                src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${responseDetail.pokemon.id}.png`}
                alt={name}
                className="image-ava"
                onClick={() => catchPokemon(responseDetail.pokemon)}
              />
              <p className="mb-2 help is-danger">
                Wanna catch me?! Click a Picture!
              </p>
            </div>
            <Type types={responseDetail.pokemon.types} />

            <InfoSection className="column info-bottom mb-4">
              <BasicInfo pokemon={responseDetail.pokemon} />
              <Stats stats={responseDetail.pokemon.stats} />
            </InfoSection>

            <NickNameForm
              isOpen={isOpenNickName}
              onClose={() => setOpenNickName(false)}
            />
          </>
        )}
      </Content>
    </Layout>
  );
};

export default Pokemon;
