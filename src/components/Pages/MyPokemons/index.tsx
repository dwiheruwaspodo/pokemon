import { FC } from "react";
import Layout from "components/Templates/Layout";
import MyPokemonList from "components/Organisms/MyPokemonList";
import NavMyPokemon from "components/Atoms/NavMyPokemon";
import { Content } from "components/Pages/Pokemons";

const MyPokemons: FC = () => {
  return (
    <Layout>
      <Content>
        <NavMyPokemon />
        <MyPokemonList />
      </Content>
    </Layout>
  );
};

export default MyPokemons;
