import React, { FC } from "react";
import Bucket from "components/Atoms/Bucket";

const NavPokemons: FC = () => {
  return (
    <div className="column has-text-centered info-top mt-6">
      <div className="columns is-mobile ml-2 mr-2">
        <div className="column has-text-left" />
        <div className="column has-text-right">
          <Bucket />
        </div>
      </div>
    </div>
  );
};

export default NavPokemons;
