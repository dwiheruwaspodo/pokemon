import React, { MouseEvent, ReactNode, FC } from "react";
import cx from "classnames";

interface ModalProps {
  children: ReactNode;
  isOpen: boolean;
  onClose: (event: MouseEvent) => void;
  className?: string;
}

const Modal: FC<ModalProps> = ({ children, isOpen, onClose, className }) => {
  return (
    <div
      className={cx("modal", className, {
        "is-active": isOpen,
      })}
    >
      <div className="modal-background" onClick={onClose} />

      <div className="modal-content">{children}</div>
    </div>
  );
};

Modal.defaultProps = {
  className: "",
};

export default Modal;
