import React, { FC } from "react";
import { useHistory } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import { RootState } from "states/store";

interface BucketProps {
  total: number;
}

export const BucketComponent: FC<BucketProps> = ({ total }) => (
  <>
    <i className="fas fa-suitcase" style={{ color: "#00d1b2" }} />
    <p className="is-success help ml-1">{total}</p>
  </>
);

const Bucket: FC = () => {
  const history = useHistory();

  const { myPokemons } = useSelector(
    (state: RootState) => ({
      myPokemons: state.pokemons.myPokemons,
    }),
    shallowEqual
  );

  return (
    <>
      <button
        className="button is-rounded"
        style={{ cursor: "pointer" }}
        onClick={() => history.push("/my-pokemon")}
      >
        <BucketComponent total={myPokemons.length} />
      </button>
    </>
  );
};

export default Bucket;
