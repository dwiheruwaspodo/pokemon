import React, { FC } from "react";
import { useHistory } from "react-router-dom";
import { Title } from "components/Pages/Pokemons";

const NavMyPokemon: FC = () => {
  const history = useHistory();

  return (
    <div className="column has-text-centered info-top mt-6">
      <div className="columns is-mobile ml-2 mr-2">
        <div className="column has-text-left is-one-fifth">
          <button
            className="button is-rounded"
            style={{ cursor: "pointer" }}
            onClick={() => history.goBack()}
          >
            <i
              className="fas fa-chevron-left fa-lg"
              style={{ color: "#00d1b2" }}
            />
          </button>
        </div>
        <div className="column has-text-centered">
          <Title className="title">My Pokemon</Title>
        </div>
        <div className="column has-text-right is-one-fifth" />
      </div>
    </div>
  );
};

export default NavMyPokemon;
