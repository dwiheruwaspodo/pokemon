import React, { FC } from "react";
import { useHistory } from "react-router-dom";
import Bucket from "components/Atoms/Bucket";

const Nav: FC = () => {
  const history = useHistory();

  return (
    <div className="column has-text-centered info-top mt-6">
      <div className="columns is-mobile ml-2 mr-2 ">
        <div className="column has-text-left">
          <button
            className="button is-rounded"
            style={{ cursor: "pointer" }}
            onClick={() => history.push("/")}
          >
            <i
              className="fas fa-chevron-left fa-lg"
              style={{ color: "#00d1b2" }}
            />
          </button>
        </div>
        <div className="column has-text-right">
          <Bucket />
        </div>
      </div>
    </div>
  );
};

export default Nav;
