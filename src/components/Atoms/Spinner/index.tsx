import React, { FC, ReactNode } from "react";
import styled from "@emotion/styled";

interface ComponentProps {
  children: ReactNode;
}

const Component: FC<ComponentProps> = ({ children }) => <div>{children}</div>;

const SpinnerComponent = styled(Component)`
  display: inline-block;
  position: relative;
  width: 80px;
  height: 80px;

  div {
    position: absolute;
    // border: 4px solid #ff884b;
    border: 4px solid #1b75bb;
    opacity: 1;
    border-radius: 50%;
    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
  }

  div:nth-child(2) {
    animation-delay: -0.5s;
  }

  @keyframes {
    0% {
      top: 36px;
      left: 36px;
      width: 0;
      height: 0;
      opacity: 1;
    }
    100% {
      top: 0px;
      left: 0px;
      width: 72px;
      height: 72px;
      opacity: 0;
    }
  }
`;

const Spinner: FC = () => (
  <SpinnerComponent>
    <div />
    <div />
  </SpinnerComponent>
);

export default Spinner;
