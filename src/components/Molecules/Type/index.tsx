import React, { FC } from "react";
import { TType } from "types/pokemon";
import s from "shortid";

interface TypeProps {
  types: TType[];
}
const Type: FC<TypeProps> = ({ types }) => {
  return (
    <div className="columns ability is-multiline is-mobile has-text-centered">
      {types.map((value) => {
        return (
          <div className="column" key={s.generate()}>
            <button className="button is-success is-rounded is-small">
              {value.type.name}
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default Type;
