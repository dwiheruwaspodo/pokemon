import React, { FC } from "react";
import { TPokemonItem } from "types/pokemon";
import "./CardItem.scss";
import { useHistory } from "react-router-dom";

interface CardItemProps {
  pokemon: TPokemonItem;
}
const CardItem: FC<CardItemProps> = ({ pokemon }) => {
  const history = useHistory();
  const click = (name: string) => {
    history.push(`/pokemon/${name}`);
  };

  return (
    <div
      className="card-item column is-one-quarter"
      onClick={() => click(pokemon.name)}
    >
      <div className="columns is-mobile">
        <div className="column is-one-quarter ava">
          <img src={pokemon.artwork} alt={pokemon.name} />
        </div>
        <div className="column title">
          <h1 className="title is-4">{pokemon.name}</h1>
        </div>
        <div className="column is-one-fifth title">
          <i className="fas fa-chevron-right" style={{ color: "#00d1b2" }} />
        </div>
      </div>
    </div>
  );
};

export default CardItem;
