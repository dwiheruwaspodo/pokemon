import React, { FC } from "react";
import { TStat } from "types/pokemon";
import s from "shortid";

interface StatProps {
  stats: TStat[];
}
const Stats: FC<StatProps> = ({ stats }) => {
  return (
    <div className="column mt-0 ml-0 mb-3">
      <h1 className="title is-4 mb-1">Stats</h1>
      {stats.map((value) => {
        return (
          <div className="field pr-3" key={s.generate()}>
            <label className="label" style={{ fontWeight: 400 }}>
              {value.stat.name} ({value.base_stat})
            </label>
            <div className="control">
              <progress
                className="progress is-success"
                value={value.base_stat}
                max="100"
              />
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Stats;
