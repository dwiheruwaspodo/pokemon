import React, { FC } from "react";
import { TPokemon } from "types/pokemon";
import "./MyPokemonCard.scss";
import { actions } from "states/slice";
import { useAppDispatch } from "hooks";
import { shallowEqual, useSelector } from "react-redux";
import { RootState } from "states/store";
import { toastr } from "react-redux-toastr";

interface MyPokemonCardProps {
  pokemon: TPokemon;
}
const MyPokemonCard: FC<MyPokemonCardProps> = ({ pokemon }) => {
  const dispatch = useAppDispatch();
  const { myPokemons } = useSelector(
    (state: RootState) => ({
      myPokemons: state.pokemons.myPokemons,
    }),
    shallowEqual
  );

  const remove = (nickname: string) => {
    let tempMyPokemon = Object.assign([], myPokemons);
    tempMyPokemon = tempMyPokemon.filter(
      (v: TPokemon) => v.nickname !== nickname
    );

    dispatch(actions.setMyPokemon(tempMyPokemon));
    toastr.success("", `${nickname} already deleted`);
  };

  return (
    <div className="card-item column is-one-quarter">
      <div className="columns is-mobile">
        <div className="column is-one-quarter ava">
          <img
            src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.id}.png`}
            alt={pokemon.name}
          />
        </div>
        <div className="column title">
          <h1 className="title is-4">{pokemon.name}</h1>
          <p className="title mb-2 help is-success">{pokemon.nickname}</p>
        </div>
        <div
          className="column is-one-fifth title"
          onClick={() => remove(pokemon.nickname)}
        >
          <i className="fas fa-trash-alt" style={{ color: "#00d1b2" }} />
        </div>
      </div>
    </div>
  );
};

export default MyPokemonCard;
