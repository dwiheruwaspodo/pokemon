import { FC, ReactNode } from "react";
import cx from "classnames";
import "./Layout.scss";

interface TLayoutProps {
  children: ReactNode;
}
const Layout: FC<TLayoutProps> = ({ children }) => {
  return <main className={cx("main")}>{children}</main>;
};

export default Layout;
