export interface TPokemon {
  id: number;
  abilities: TAbility[];
  name: string;
  species: TBaseName;
  message: string;
  weight: number;
  height: number;
  held_items: THeldItems[];
  sprites: TSprite;
  stats: TStat[];
  types: TType[];
  nickname: string;
}

export interface TStat {
  base_stat: number;
  effort: number;
  stat: TBaseName;
}

export interface TType {
  slot: number;
  type: TBaseName;
}

interface THeldItems {
  item: TBaseName;
}

interface TSprite {
  back_default: string;
  back_female: string;
  back_shiny: string;
  back_shiny_female: string;
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
}

export interface TAbility {
  ability: TBaseName;
  is_hidden: boolean;
  slot: number;
}

interface TBaseName {
  id: number;
  url: string;
  name: string;
}

export interface TPokemonItem {
  id: number;
  url: string;
  name: string;
  image: string;
  artwork: string;
  dreamworld: string;
}

interface TPokemonListResponse {
  count: number;
  next: string;
  previous: string;
  nextOffset: number;
  prevOffset: number;
  params: string;
  results: TPokemonItem[];
  status: boolean;
  message: boolean;
}

export interface ResponsePokemonList {
  pokemons: TPokemonListResponse;
}

export interface ResponsePokemonDetail {
  pokemon: TPokemon;
}
