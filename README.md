# Pokemon Apps

Aplikasi buat nampilin pokemon, kamu jugak bisa nangkep pokemon sendiri diaplikasi ini.

Lihat versi deploy divercel : https://pokemon-waspodov.vercel.app


## How to run in local?

Sebelum dijalanin, diinstall duluk gaes:

### `yarn install`

Setelah diinstall, run pakek ini:

### `yarn start`

## Stack used
* Reactjs
    * Typescript
    * Redux toolkit
* Bulma SCSS
* Emotion CSS-in-JS
* GraphQl
* Lodash

